package com.firstecxample.ermin;

import java.util.UUID;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ErminApplication {

	public static void main(String[] args) {
		SpringApplication.run(ErminApplication.class, args);
	}

}
