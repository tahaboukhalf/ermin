package com.firstecxample.ermin.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.firstecxample.ermin.Model.Coffee;
import com.firstecxample.ermin.repository.CoffeeRepository;
import com.sun.el.stream.Optional;

@RestController
@RequestMapping("/coffees")
public class RestApiDempController {

	private final CoffeeRepository cofRepo;
	
	private List<Coffee> coffees= new ArrayList<>();
	
	public RestApiDempController(CoffeeRepository cofRepo) {
		this.cofRepo= cofRepo;
		coffees.add(new Coffee("Java"));
		coffees.add(new Coffee("ness ness"));
		coffees.add(new Coffee("koko"));
		coffees.add(new Coffee("capu"));
		this.cofRepo.saveAll(coffees);
		
	}
	@GetMapping
	Iterable<Coffee> getCoffees(){
		return coffees;
	}
	@GetMapping("/{id}")
	java.util.Optional<Coffee> getCoffeeByItsId(@PathVariable String id) {
		for(Coffee i : coffees) {
			if(id.equals(i.getId())) {
				return java.util.Optional.of(i);
			}
		}
		return java.util.Optional.empty();
	}

	@PostMapping
	public Coffee addCoffee(@RequestBody Coffee cof) {
		coffees.add(cof);
		return cof;
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Coffee> modifyCoffee(@PathVariable String id ,@RequestBody Coffee cof) {
		int index = -1;
		for(Coffee c : coffees) {
			if(c.getId().equals(id)) {
				index = coffees.indexOf(c);
				coffees.set(index, cof);
			}
		}
		return (index == -1) ?
				new ResponseEntity<>(addCoffee(cof),HttpStatus.CREATED):
					new ResponseEntity<>(cof,HttpStatus.OK);
	}
	
	
	@DeleteMapping("/{id}")
	public void deleteCoffee(@PathVariable String id) {
		coffees.removeIf(c -> c.getId().contentEquals(id));
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
